﻿using UnityEngine;
using UnityEngine.UI;

public class AuthenticationImage : MonoBehaviour
{
    public Image image;
    public Sprite lockedSprite;
    public Sprite unlockedSprite;
    public AuthenticationEnum id;

    public void OnEnable()
    {
        bool authenticated = PlayerPrefs.GetString(id.ToString()) == AuthenticationManager.STATUS_AUTHENTICATED;

        Debug.Log(id + " is " + authenticated);

        image.sprite = authenticated ? unlockedSprite : lockedSprite;
    }
}
