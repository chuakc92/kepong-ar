﻿public enum AuthenticationEnum
{
    Others = -3,
    Video= -2,
    Map,
    Bottling=0,
    Filter,
    Ferment,
    Wort,
    Malt
}
