﻿using UnityEngine;
using UnityEngine.UI;

public class AudioController : MonoBehaviour
{
    public AudioSource source;
    private bool soundOn = true;

    public Sprite soundOnSprite;
    public Sprite soundOffSprite;

    public Image soundButtonImage;

    private void Awake()
    {
        if(!PlayerPrefs.HasKey("Sound"))
        {
            PlayerPrefs.SetInt("Sound", 1);
        }
        else if(PlayerPrefs.HasKey("Sound"))
        {
            if(PlayerPrefs.GetInt("Sound") == 1)
            {
                soundOn = true;
                source.Play();
                soundButtonImage.sprite = soundOnSprite;
            }
            else
            {
                soundOn = false;
                source.Stop();
                soundButtonImage.sprite = soundOffSprite;
            }
        }
    }

    public void ToggleOnOff()
    {
        soundOn = !soundOn;

        if(soundOn)
        {
            PlayerPrefs.SetInt("Sound", 1);
            source.Play();
            soundButtonImage.sprite = soundOnSprite;
        }
        else
        {
            PlayerPrefs.SetInt("Sound", 0);
            source.Stop();
            soundButtonImage.sprite = soundOffSprite;
        }
    }
}
