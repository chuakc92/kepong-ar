﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class TriviaController : MonoBehaviour
{
    public List<TextMeshPro> triviaTexts;

    private void Awake()
    {
        triviaTexts.ForEach(x => x.gameObject.SetActive(false));
    }

    public void ToggleOn()
    {
        triviaTexts.ForEach(x => x.gameObject.SetActive(true));
    }
}
