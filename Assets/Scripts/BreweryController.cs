﻿using UnityEngine;

public class BreweryController : MonoBehaviour
{
    public AuthenticationEnum id;

    public GameObject locked;
    public GameObject unlocked; 

    private void OnEnable()
    {
        locked.SetActive(false);
        unlocked.SetActive(false);

        string s = ((AuthenticationEnum)id).ToString();

        if (PlayerPrefs.GetString(s) == AuthenticationManager.STATUS_AUTHENTICATED)
        {
            unlocked.SetActive(true);
        }
        else locked.SetActive(true);
    }
}
