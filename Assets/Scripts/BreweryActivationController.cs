﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BreweryActivationController : MonoBehaviour
{
    public List<GameObject> objsToActivate;
    public List<GameObject> objsToInactivate;

    public GameObject breweryPanel;

    public bool activate = false;

    public void ToggleStaticModel()
    {     
        if (objsToActivate[0].activeInHierarchy)
        {
            objsToActivate.ForEach(x => x.SetActive(false));
        }
        else objsToActivate.ForEach(x => x.SetActive(true));
    }

    public void ToggleInactive()
    {
        /*if (breweryPanel.activeInHierarchy)
        {
            breweryPanel.SetActive(false);
            return;
        }*/


        if (objsToActivate[0].activeInHierarchy)
        {
            objsToActivate.ForEach(x => x.SetActive(false));
        }
        else
        {
            //breweryPanel.SetActive(true);
        }

        objsToInactivate.ForEach(x => x.SetActive(false));
    }
}
