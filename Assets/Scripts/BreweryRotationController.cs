﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BreweryRotationController : MonoBehaviour
{
    public Transform brewery;

    public void OnHorizontalButtonPressed(float x)
    {
        //Debug.Log("Horizontal Pressed");
        BreweryInputManager.rotate = x;
    }

    public void OnHorizontalButtonReleased()
    {
        //Debug.Log("Horizontal Released");
        BreweryInputManager.rotate = 0;
    }

    public void OnVerticalButtonPressed(float y)
    {
        //Debug.Log("Vertical Pressed");
        BreweryInputManager.zoom = y;
    }

    public void OnVerticalButtonReleased()
    {
        //Debug.Log("Vertical Pressed");
        BreweryInputManager.zoom = 0;
    }
}
