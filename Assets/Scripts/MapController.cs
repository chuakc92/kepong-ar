﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MapController : MonoBehaviour
{
    public Button toggleMapButton;
    public Text tapOnGroundText;
    public GameObject groundPlane;
    public GameObject planeFinder;

    private bool activate = false;

    private void Awake()
    {
        AuthenticationManager.OnAuthenticateEnum += MapAuthenticated;

        string s = AuthenticationEnum.Map.ToString();
        if (PlayerPrefs.HasKey(s) && PlayerPrefs.GetString(s) == AuthenticationManager.STATUS_AUTHENTICATED)
        {
            toggleMapButton.interactable = true;
        }
    }

    public void OnToggleButtonClicked()
    {
        activate = !activate;

        tapOnGroundText.gameObject.SetActive(activate);
        groundPlane.SetActive(activate);
        planeFinder.SetActive(true);
    }

    public void OnMapSpawned()
    {
        tapOnGroundText.gameObject.SetActive(false);
    }

    private void MapAuthenticated(AuthenticationEnum a)
    {
        if(a == AuthenticationEnum.Map)
        {
            toggleMapButton.interactable = true;
        }
    }

    private void OnDestroy()
    {
        AuthenticationManager.OnAuthenticateEnum -= MapAuthenticated;
    }
}
