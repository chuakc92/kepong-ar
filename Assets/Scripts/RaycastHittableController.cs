﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RaycastHittableController : MonoBehaviour
{
    public AuthenticationEnum type;

    public void OnHit()
    {

    }

    public void OnToggleOn()
    {
        GetComponent<TriviaController>().ToggleOn();
    }
}
