﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class AuthenticationManager : MonoBehaviour
{
    public static string STATUS_AUTHENTICATED = "Yes";
    public static string STATUS_NOT_AUTHENTICATED = "No";

    public static bool CROSSHAIR_ENABLED = false;

    private void Awake()
    {
        InitializePlayerPrefs();

        OnAuthenticateEnum += Authenticate;
    }

    private void InitializePlayerPrefs()
    {
        for (int i = -1; i < 5; i++)
        {
            string s = ((AuthenticationEnum)i).ToString();

            if (!(PlayerPrefs.HasKey(s)))
            {
                Debug.Log("Player does not have key " + s);
                PlayerPrefs.SetString(s, STATUS_NOT_AUTHENTICATED);
            }
            else Debug.Log("Player has key " + s + " that has statis " + PlayerPrefs.GetString(s));
        }
    }

    private void Authenticate(AuthenticationEnum a)
    {
        string s = a.ToString();
        if (PlayerPrefs.GetString(s) == STATUS_AUTHENTICATED) return;

        Debug.LogAssertion("Authenticating " + a);
        PlayerPrefs.SetString(s, STATUS_AUTHENTICATED);
    }

    public void SetAllNotAuthenticate()
    {
        for (int i = -1; i < 5; i++)
        {
            string s = ((AuthenticationEnum)i).ToString();

            PlayerPrefs.SetString(s, STATUS_NOT_AUTHENTICATED);
        }
    }

    public void SetAllAuthenticate()
    {
        for (int i = -1; i < 5; i++)
        {
            string s = ((AuthenticationEnum)i).ToString();

            PlayerPrefs.SetString(s, STATUS_AUTHENTICATED);
        }
    }

    public void ResetPlayerPrefs()
    {
        PlayerPrefs.DeleteAll();
    }

    private void OnDestroy()
    {
        OnAuthenticateEnum -= Authenticate;
    }

    public static Action<AuthenticationEnum> OnAuthenticateEnum;
}
