﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CrossHairController : MonoBehaviour
{
    public Image tick;
    public GameObject crossHair;

    public Color32 fadeInColor = new Color32(255, 255, 255, 255);
    public Color32 fadeOutColor = new Color32(255, 255, 255, 0);
    private Color32 imageColor;

    private bool crossHairOn = false;
    private bool fadeIn = false;

    private float t = 0;
    public float fadeTime = 0.5f;

    private void Awake()
    {
        AuthenticationManager.OnAuthenticateEnum += Authenticated;
        imageColor = tick.color;
    }

    private void Authenticated(AuthenticationEnum _enum)
    {
        if (PlayerPrefs.GetString(_enum.ToString()) == AuthenticationManager.STATUS_AUTHENTICATED)
            return;
        Debug.LogAssertion("Tick");
        fadeIn = true;
        t = 0;

        Invoke("FadeOut", 1.5f);
    }

    public void OnBreweryEnabled()
    {
        crossHairOn = false;

        AuthenticationManager.CROSSHAIR_ENABLED = crossHairOn;

        crossHair.SetActive(crossHairOn);
    }

    public void ToggleCrossHair()
    {
        crossHairOn = !crossHairOn;

        imageColor = fadeOutColor;

        AuthenticationManager.CROSSHAIR_ENABLED = crossHairOn;

        crossHair.SetActive(crossHairOn);
    }

    private void FadeOut()
    {
        fadeIn = false;
        t = 0;
    }

    private void Update()
    {
        t += Time.deltaTime / fadeTime;

        tick.color = fadeIn ? Color32.Lerp(tick.color, fadeInColor, t) : Color32.Lerp(tick.color, fadeOutColor, t);
    }

    private void OnDestroy()
    {
        AuthenticationManager.OnAuthenticateEnum -= Authenticated;
    }
}
