﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CursorRaycastController : MonoBehaviour
{
    public LayerMask layerMask;
    public Camera arCamera;

    public Button toggleTriviaButton;

    RaycastHittableController current;

    public List<AuthenticationEnum> activated;

    private void Awake()
    {
        toggleTriviaButton.onClick.AddListener(TriviaToggled);
    }

    private void TriviaToggled()
    {
        activated.Add(current.type);
        current.OnToggleOn();
    }
    // Update is called once per frame
    private void Update()
    {
        RaycastHit hit;
        Ray ray = arCamera.ScreenPointToRay(transform.position);

        if (Physics.Raycast(ray, out hit, Mathf.Infinity, layerMask))
        {
            if (current == null || current.type != hit.collider.GetComponent<RaycastHittableController>().type)
                current = hit.collider.GetComponent<RaycastHittableController>();

            if(current != null)
            {
                current.OnHit();

                if (!activated.Contains(current.type))
                    toggleTriviaButton.gameObject.SetActive(true);
                else
                    toggleTriviaButton.gameObject.SetActive(false);
            }
        }
        else toggleTriviaButton.gameObject.SetActive(false);
    }

    private void OnDrawGizmos()
    {
        Gizmos.color = Color.red;

        Vector3 direction = transform.TransformDirection(Vector3.forward) * 100f;

        Gizmos.DrawRay(transform.position, direction);
    }
}
