﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BreweryAuthenticationManager : MonoBehaviour
{
    public List<GameObject> components;

    private void OnEnable()
    {
        ActivateAuthenticated();
    }

    private void ActivateAuthenticated()
    {
        for (int i = 0; i < 5; i++)
        {
            string s = ((AuthenticationEnum)i).ToString();

            if(PlayerPrefs.GetString(s) == AuthenticationManager.STATUS_AUTHENTICATED)
            {
                components[i].SetActive(true);
            }
        }
    }
}
