﻿using System;
using UnityEngine;
using UnityEngine.UI;
public class BreweryInputManager : MonoBehaviour
{
    public Camera arCamera;
    public Transform rotationTransform;

    [Header("Speeds")]
    public float translateSpeed = 0.5f;
    public float zoomSpeed = 0.5f;
    public float rotateSpeed = 0.5f;

    [Header("Default Vectors")]
    public Vector3 normalPos;
    public Vector3 normalRot;

    [Header("Clamped Vectors")]
    public Vector3 clampedPosMin;
    public Vector3 clampedPosMax;

    public Vector3 originalClampedPosMin;
    public Vector3 originalClampedPosMax;

    public Vector3 clampedRotMin;
    public Vector3 clampedRotMax;

    private Vector2 lastPos;

    public float rotateMin = 0;
    public float rotateMax = 180;

    private bool facingOpposite = false;

    public static float rotate = 0;
    public static float zoom = 0;

    private void Awake()
    {
        normalPos = transform.localPosition;

        originalClampedPosMin = clampedPosMin;
        originalClampedPosMax = clampedPosMax;
    }

    // Update is called once per frame
    void Update()
    {
        Debug.Log(transform.localPosition);
        if (Input.touchCount == 2)
        {
            clampedPosMin = new Vector3(transform.localPosition.x, clampedPosMin.y, originalClampedPosMin.z);
            clampedPosMax = new Vector3(transform.localPosition.x, clampedPosMax.y, originalClampedPosMax.z);

            // Store both touches.
            Touch touchZero = Input.GetTouch(0);
            Touch touchOne = Input.GetTouch(1);

            // Find the position in the previous frame of each touch.
            Vector2 touchZeroPrevPos = touchZero.position - touchZero.deltaPosition;
            Vector2 touchOnePrevPos = touchOne.position - touchOne.deltaPosition;

            // Find the magnitude of the vector (the distance) between the touches in each frame.
            float prevTouchDeltaMag = (touchZeroPrevPos - touchOnePrevPos).magnitude;
            float touchDeltaMag = (touchZero.position - touchOne.position).magnitude;

            // Find the difference in the distances between each frame.
            float deltaMagnitudeDiff = prevTouchDeltaMag - touchDeltaMag;

            transform.Translate(0, 0, deltaMagnitudeDiff * zoomSpeed, Space.Self);

            Debug.Log(transform.localPosition);

            transform.localPosition = new Vector3
                                        (Mathf.Clamp(transform.localPosition.x, clampedPosMin.x, clampedPosMax.x), 
                                        Mathf.Clamp(transform.localPosition.y, clampedPosMin.y, clampedPosMax.y), 
                                        Mathf.Clamp(transform.localPosition.z, clampedPosMin.z, clampedPosMax.z)
                                        );
        }
        else if(Input.touchCount == 1)
        {
            clampedPosMin = new Vector3(originalClampedPosMin.x, clampedPosMin.y, transform.localPosition.z);
            clampedPosMax = new Vector3(originalClampedPosMax.x, clampedPosMax.y, transform.localPosition.z);

            Touch touchZero = Input.GetTouch(0);

            if(touchZero.phase == TouchPhase.Began)
            {
                lastPos = touchZero.position;
            }
            if(touchZero.phase == TouchPhase.Moved)
            {
                Vector3 delta = (touchZero.position - lastPos);

                bool swipeRight = touchZero.position.x > lastPos.x;

                float magnitude = delta.magnitude * translateSpeed;

               // if (facingOpposite) swipeRight = !swipeRight;

                transform.Translate(swipeRight ? magnitude : -magnitude, 0, 0, Space.Self);

                transform.localPosition = new Vector3
                (Mathf.Clamp(transform.localPosition.x, clampedPosMin.x, clampedPosMax.x),
                Mathf.Clamp(transform.localPosition.y, clampedPosMin.y, clampedPosMax.y),
                Mathf.Clamp(transform.localPosition.z, clampedPosMin.z, clampedPosMax.z)
                );

                lastPos = touchZero.position;

                Debug.Log(transform.localPosition);
            }
        }
    }

    private void LateUpdate()
    {
        Quaternion desiredRotation = rotationTransform.localRotation;

        DetectTouchMovement.Calculate();

        if (Mathf.Abs(DetectTouchMovement.turnAngleDelta) > 0)
        { // rotate
            Vector3 rotationDeg = Vector3.zero;
            rotationDeg.y = -DetectTouchMovement.turnAngleDelta;
            desiredRotation *= Quaternion.Euler(rotationDeg);
        }
        rotationTransform.localRotation = desiredRotation;
    }

    private void OnEnable()
    {
        transform.localPosition = normalPos;
       // transform.localRotation = Quaternion.Euler(normalRot);
    }
}



