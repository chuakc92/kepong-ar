﻿using UnityEngine;
using Vuforia;

public class MidAirPositionerInitialiser : MonoBehaviour
{
    void Start()
    {
        var vuforia = VuforiaARController.Instance;

        vuforia.RegisterVuforiaStartedCallback(OnVuforiaStarted);
    }

    private void OnVuforiaStarted()
    {
        FindObjectOfType<MidAirPositionerBehaviour>().enabled = true;
    }
}
